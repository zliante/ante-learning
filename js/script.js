$(document).ready(function () {
    /* Header resizing */

    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.header-container').addClass('header-container--sticky');
            $('.logo').addClass('logo--small');
        } else if ($(this).scrollTop() < 50) {
            $('.logo').removeClass('logo--small');
            $('.header-container').removeClass('header-container--sticky');
        }

    });

    /* Responsive menu */
    $("#open-menu").click(function () {
        $(".navigation-holder__mobile-menu").toggleClass("navigation-holder__mobile-menu--open");
    })
});
